set OLD_PATH=%PATH%
if exist %DW_INSTALL_DIR%DeltaWorks\jre142 goto GOOD
set JAVA_HOME=%DW_INSTALL_DIR%\DeltaWorks\jre142
goto START
:GOOD
set JAVA_HOME=%DW_INSTALL_DIR%DeltaWorks\jre142
:START
set PATH=%JAVA_HOME%\bin;%path%
java -cp .;AgentConnectDemoConnector.jar com.moneygram.acdc.AgentConnectDemoConnector -port 4545 -msgConfig configuration/configuration.txt
set PATH=%OLD_PATH%