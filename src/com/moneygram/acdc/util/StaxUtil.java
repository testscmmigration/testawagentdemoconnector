/*
 *	Copyright 2005 stat4j.org
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.moneygram.acdc.util;

import java.util.LinkedList;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import com.moneygram.acdc.ACDCException;
import com.moneygram.acdc.XMLElement;

/**
 * @author Lara D'Abreo
 * 
 * Common stax utilities.
 *  
 */
public class StaxUtil {

    public static void moveReaderToElement(String target, XMLStreamReader reader)
            throws XMLStreamException {

        // If current element is equal to target

        String readElement = null;
        for (int event = reader.next(); event != XMLStreamConstants.END_DOCUMENT; event = reader
                .next()) {

            if ((event == XMLStreamConstants.START_ELEMENT)
                    && (reader.getLocalName().equals(target))) {
                return;
            }
        }
    }

    public static void writeElement(XMLStreamWriter writer, String elementName,
            String value) throws Exception {
        writer.writeStartElement(elementName);
        writer.writeCharacters(value);
        writer.writeEndElement();
    }

    
    /**
     * Started at START_ELEMENT of hierarchy
     * 
     * @param parser
     * @param payload
     * @param currentName
     * @throws ACDCException
     */
    public static XMLElement loadNamedTags(XMLStreamReader parser, XMLElement root)
            throws ACDCException {

        LinkedList elements = new LinkedList();
        String value = "";
        String currentName;

        try {
            //Add the initial 
            elements.addLast(root);
            
            while (parser.hasNext()) {
                int innerEvent = parser.next();
                if (innerEvent == XMLStreamConstants.START_ELEMENT) {
                    currentName= parser.getLocalName();
                    XMLElement e = new XMLElement(currentName);
                    elements.addLast(e);
                    int c = parser.getAttributeCount();
                    for (int i = 0; i < c; i++) {
                    	String an = parser.getAttributeName(i).getLocalPart();
                    	String av = parser.getAttributeValue(i);
                    	e.addAttribute(an, av);
                    }
                    value = "";
                } else if (innerEvent == XMLStreamConstants.CHARACTERS
                        || innerEvent == XMLStreamConstants.SPACE) {
                    String parserText = parser.getText();
                    if (parserText != null) {
                        parserText = parserText.trim();
                        if (parserText.length() > 0)
                            value += parser.getText();
                    }
                } else if (innerEvent == XMLStreamConstants.END_ELEMENT) {
                    if (elements.size() > 0){
                        XMLElement currentElem = ((XMLElement)elements.getLast()); 
                        if( value.length() > 0) {
	                        currentElem.setValue(value);
	                        value = "";
                        }
                        
                        elements.removeLast();
                        
                        if (elements.size() > 0)
                            ((XMLElement)elements.getLast()).addChild(currentElem);
                        else
                            break;
                    }
                    else {
                        //If we don't know about this start tag, then we must
                        // be done.
                        break;
                    }
                }
            }
        } catch (XMLStreamException ex) {
            throw new ACDCException("Failed to parser tag", ex);
        }

        return root;
    }
}
