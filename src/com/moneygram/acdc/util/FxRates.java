package com.moneygram.acdc.util;

import java.text.NumberFormat;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.moneygram.acdc.ACDCMessageConfigXMLHandler;

public class FxRates {

	public static final Logger log = Logger
			.getLogger(ACDCMessageConfigXMLHandler.class);

	private static HashMap fxRates = new HashMap();

	public static void addRxRate(String sCurrency, String sRate) {
		log.info("    Currency[" + sCurrency + "],Rate[" + sRate + "]");
		fxRates.put(sCurrency, sRate);
	}

	public static String GetFxRate(String sSendCurr, String sRecvCurr) {

		final float fRate = getRate(sRecvCurr) / getRate(sSendCurr);
		final NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(6);
		nf.setGroupingUsed(false);
		return nf.format(fRate);
	}

	private static float getRate(String sCurr) {
		float fRetValue = (float) 1.5;
		final String sRate = (String) fxRates.get(sCurr);
		if (sRate != null) {
			try {
				final float fRate = Float.valueOf(sRate).floatValue();
				fRetValue = fRate;
			} catch (final NumberFormatException e) {
			}
		}
		return fRetValue;
	}

}
