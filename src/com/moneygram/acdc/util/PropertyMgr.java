/*
 * Created on Jan 19, 2005
 *
 */

package com.moneygram.acdc.util;

import java.io.*;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
*  This class loads a properties file (and potential default file) 
*  once the first time it's called and then holds the contents in memory 
*  for quick access.  
*  <P>
*  It will look for the properties file first
*  using getResourceAsStream(). If it
*  is unable to locate the properties file that way, next it will search
*  based on the environment variable 'property_path' which can be set at
*  startup using java's -D command line options, for ex.
*  <UL>  
*  <LI> -Dproperty_path="D:\AgentConnect\src\classes" for an absolute path 
*  <LI> or -Dproperty_path="..\props"  for a relative path
*  </UL>
*
*  If it can't find the file in the %property_path%, or if property_path 
*  env variable is not set, it will look for filename in the current 
*  working directory. 
*  <P>
*
*  For convenience, for numerical properties you can use the getInt, getLong,
*  getFloat, or getDouble methods.  
*
*  Extensions of this class can force it to do another read from file 
*  by calling PropertyMgr.load().
*  <p>  
* 
*/

public class PropertyMgr implements PropertyKeys{
	private static Logger log =
		Logger.getLogger(PropertyMgr.class);
	//Use java's platform independent separtor
	protected static final String SEPARATOR = java.io.File.separator;

	private Properties myPropertiesTable;
	private String filename;
	private String defaultFilename;
	private boolean loaded = false;

	public PropertyMgr(String filename) {
		this.filename = filename;
	}

	public PropertyMgr(String defaultFileName, String filename) {
		this.filename = filename;
		this.defaultFilename = defaultFileName;
	}
	private InputStream getFileInputStream(String fileNameToLoad)
		throws FileNotFoundException, SecurityException, IOException {
		FileInputStream fileInputStream = null;
		File file = new File(fileNameToLoad);
		//Next, check to see if the PROPERTY_FILE_PATH 
		// environment variable is set, if so, use that 
		// as the directory to look in for the properties
		// file
		String propPath = System.getProperty(PROPERTY_FILE_PATH);
		log.debug("Response of getting System prop '"+ PROPERTY_FILE_PATH+ "' is "+ propPath);
		String workingDir = System.getProperty("user.dir");
		log.debug("Current working dir is " + workingDir);

		if (propPath != null) {
			try {
				propPath = propPath.trim();

				String propFile;
				if (propPath.equals(".")) {
					// If someone explicitly sets propPath to current dir, 
					// don't append a separator or the "."
					propFile = fileNameToLoad;
				} else {
					propFile = propPath + SEPARATOR + fileNameToLoad;
				}
				log.debug("FOUND env var: " + PROPERTY_FILE_PATH + " try to load file " + propFile);
				fileInputStream = new FileInputStream(propFile);
				log.debug("FOUND env var: " + PROPERTY_FILE_PATH + " loaded file " + propFile);
			} catch (java.io.FileNotFoundException e) {
				log.debug(
					PROPERTY_FILE_PATH + " env variable not set correctly,"
						+ " trying to locate " + fileNameToLoad	+ " in current working dir");
			}
		}
		
		//Check if we found the prop file using the env var
		if (fileInputStream == null) {
			//didn't find it using env var - try to locate in working dir
			log.debug(
				"Couldn't find file "
					+ fileNameToLoad + " using " + PROPERTY_FILE_PATH
					+ " env var, instead try to find in current working dir: "+ workingDir);
			fileInputStream = new FileInputStream(file);
		}
		
		return fileInputStream;
	}

	private Properties getPropertiesForFile(
		String fileNameToLoad,
		Properties defaultProperties)
		throws FileNotFoundException, SecurityException, IOException {

		if (fileNameToLoad == null)
			return null;

		InputStream classPathInputStream = null;
		Properties propsToReturn = null;
		if (defaultProperties != null)
			propsToReturn = new Properties(defaultProperties);
		else
			propsToReturn = new Properties();

		log.debug("Try using PropertyMgr.class.getResourceAsStream");
		//Make sure the path starts with an absolute.
		String tempPath = fileNameToLoad;
		if (!tempPath.startsWith("/"))
			tempPath = "/" + tempPath;
		classPathInputStream =
			PropertyMgr.class.getResourceAsStream(tempPath);
		if (classPathInputStream != null) {
			try {
				log.debug("Attempting to load "	+ fileNameToLoad + " using classpath");
				propsToReturn.load(classPathInputStream);
				log.debug("SUCCESSFULLY loaded " + fileNameToLoad + " using classpath");
			}
			finally {
				if (classPathInputStream != null){
					try {
						classPathInputStream.close();
					} catch (IOException e) {
						log.debug("Error closing file", e);
					}
				}
			}
		} else {
			InputStream fileInputStream = null;
			try {
				log.debug("Could not find properties via classpath.  Attempting filesystem load.");
				fileInputStream = getFileInputStream(fileNameToLoad);
				log.debug("Found input stream for file: " + fileNameToLoad+". Attempting properties load.");
				propsToReturn.load(fileInputStream);
				log.debug("SUCCESSFULLY loaded " + fileNameToLoad + " using filesystem.");
			} finally {
				if (fileInputStream != null) {
					// have to close the file, to allow file system changing of the file.
					try {
						fileInputStream.close();
					} catch (IOException e) {
						log.debug("Error closing file", e);
					}
				}
			}
		}

		log.debug("Successfully loaded file " + fileNameToLoad);

		return propsToReturn;
	}

	protected synchronized void load()
		throws FileNotFoundException, SecurityException, IOException {

		Properties defProps = null;
		if (defaultFilename != null) {
			try {
				defProps = getPropertiesForFile(defaultFilename, null);
			} catch (Exception ex) {
				log.warn(
					"Could not load default properties: " + defaultFilename,
					ex);
			}
		}

		try {
			myPropertiesTable = getPropertiesForFile(filename, defProps);
		} catch (Exception ex) {
			log.error("Could not load properties for: " + filename, ex);
		} finally {
			//We only try to load once.
			loaded = true;
		}

	}

	private synchronized void loadIfNeeded() {
		if (!loaded) {
			try {
				load();
			} catch (Exception ex) {
				log.error("Failed to load: "+ filename+ ", default: "+ defaultFilename);
			}
		}
	}

	/**
	 * Returns the value of a named property from 
	 * the properties file, or null if  property does not exist.
	 * @return String - the value of a named property from 
	 *      the properties file, or null if  property does not exist.
	 */
	public String getProperty(String name) {
		loadIfNeeded();

		String s = myPropertiesTable.getProperty(name);
		if (s == null) {
			return null;
		} else {
			return s.trim();
		}
	}

	/**
	 * Returns the value of a named property from 
	 * the properties file, or null if  property does not exist.
	 * @return String - the value of a named property from 
	 *      the properties file, or the default value if  property does not exist.
	 */
	public String getProperty(String name, String defaultValue) {
		String value = getProperty(name);
		if (value == null)
			return defaultValue;	 
		
		return value;
	}
	
	/**
	* Attempts to get the property specified by param 'name' and 
	* parse it as an int.  
	* @param String name - the name of the property you want to retrieve
	* @return int - if successfully able to parse as an int
	* @throws NumberFormatException - if either the property doesn't exist
	*  (is null) or the parsing fails
	*/
	public int getIntProperty(String name) throws NumberFormatException {
		String ss = getProperty(name);
		if (ss != null) {
			return Integer.parseInt(ss);
		} else {
			throw new NumberFormatException("Can't convert null to an int");
		}
	}

	/**
	* Attempts to get the property specified by param 'name' and 
	* parse it as a long.  
	* @param String name - the name of the property you want to retrieve
	* @return long - if successfully able to parse as a long
	* @throws NumberFormatException - if either the property doesn't exist
	*  (is null) or the parsing fails
	*/
	public long getLongProperty(String name) throws NumberFormatException {
		String ss = getProperty(name);
		if (ss != null) {
			return Long.parseLong(ss);
		} else {
			throw new NumberFormatException("Can't convert null to an long");
		}
	}

	/**
	* Attempts to get the property specified by param 'name' and 
	* parse it as a float.  
	* @param String name - the name of the property you want to retrieve
	* @return float - if successfully able to parse as an double
	* @throws NumberFormatException - if either the property doesn't exist
	*  (is null) or the parsing fails
	*/
	public float getFloatProperty(String name) throws NumberFormatException {
		String ss = getProperty(name);
		if (ss != null) {
			return Float.parseFloat(ss);
		} else {
			throw new NumberFormatException("Can't convert null to a float");
		}
	}

	/**
	* Attempts to get the property specified by param 'name' and 
	* parse it as an double.  
	* @param String name - the name of the property you want to retrieve
	* @return double - if successfully able to parse as an double
	* @throws NumberFormatException - if either the property doesn't exist
	*  (is null) or the parsing fails
	*/
	public double getDoubleProperty(String name) throws NumberFormatException {
		String ss = getProperty(name);
		if (ss != null) {
			return Double.parseDouble(ss);
		} else {
			throw new NumberFormatException("Can't convert null to a double");
		}
	}

	/**
	 * Given a property name, get a stream for it.
	 * @param name
	 * @return
	 * @throws IOException
	 */
	public InputStream getPropertyAsInputStream(String name)
		throws IOException {
		String prop = getProperty(name);
		if (prop == null)
			return null;

		return PropertyMgr.class.getResourceAsStream(prop);
	}

    public InputStream getPVAsInputStream(String propertyName) 
    	throws IOException {

		return PropertyMgr.class.getResourceAsStream(propertyName);
    }
    	
    
	public Properties getPropertyAsProperties(String name) throws IOException {
		InputStream is = getPropertyAsInputStream(name);
		if (is != null) {
			try {
				Properties p = new Properties();
				p.load(is);
				return p;
			}
			finally {
				try {
					is.close();
				} catch (IOException iex) {
					log.debug("Could not close file for tag: "+name);
				}
			}
		}
		
		return null;
	}
	
	public PropertyMgr getPropertyAsPropertyMgr(String name) throws IOException {
		String filename = getProperty(name);
		
		if (filename != null) {
			PropertyMgr p = new PropertyMgr(filename);
			p.load();
			return p;
		}
		
		return null;
	}
}
