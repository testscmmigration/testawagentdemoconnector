/*
 * Created on Jul 11, 2006
 *
 */
package com.moneygram.acdc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLStreamWriter;

import org.apache.log4j.Logger;

/**
 * @author T007
 *
 */
public class Payload {
	private static Logger log = Logger.getLogger(Payload.class);
    private String apiName;
    private String namespace;
    private String apiVersion;
    private XMLElement root;
    
    
    
    /**
     * @param apiName
     * @param namespace
     * @param apiVersion
     */
    public Payload(String apiName, String namespace, String apiVersion) {
        super();
        this.apiName = apiName;
        this.namespace = namespace;
        this.apiVersion = apiVersion;
        root = new XMLElement(apiName);

    }
    /**
     * @param apiName
     * @param namespace
     * @param root
     */
    public Payload(String apiName, String namespace, XMLElement root) {
        super();
        this.apiName = apiName;
        this.namespace = namespace;
        this.root = root;
    }
    
    /**
     * @param apiName
     * @param namespace
     * @param apiVersion
     * @param root
     */
    public Payload(String apiName, String namespace, String apiVersion,
            XMLElement root) {
        super();
        this.apiName = apiName;
        this.namespace = namespace;
        this.apiVersion = apiVersion;
        this.root = root;
    }
    
    /**
     * @return Returns the apiName.
     */
    public String getApiName() {
        return apiName;
    }
    /**
     * @param apiName The apiName to set.
     */
    public void setApiName(String apiName) {
        this.apiName = apiName;
    }
    /**
     * @return Returns the apiVersion.
     */
    public String getApiVersion() {
        return apiVersion;
    }
    /**
     * @param apiVersion The apiVersion to set.
     */
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
    /**
     * @return Returns the namespace.
     */
    public String getNamespace() {
        return namespace;
    }
    /**
     * @param namespace The namespace to set.
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }
    
    
    public void outputXML(XMLStreamWriter parser, String xmlPrefix, String namespace) throws ACDCException{
        Iterator i = root.getAllChildren();
        while (i.hasNext()) {
            ((XMLElement)i.next()).outputXML(parser, xmlPrefix, namespace);
        }
    }
    
    public List getEntries(String connonicalName) {
        ArrayList retList = new ArrayList();
        getEntries(connonicalName, null, retList);
        return retList;
    }


    private void getEntries(String cannonicalName, List children, List retList)  {
    	boolean isSuccess=false;
    	try {
        	log.info("Processing tag-"+cannonicalName+"["+this.apiName+"]");
        	
			int nameIndex = cannonicalName.indexOf(".");
			if (nameIndex == -1) {

			    if (children == null) {
			        children = root.getChild(cannonicalName);
			        if (children != null) {
			        	retList.addAll(children);
						isSuccess=true;
			        }
			    }
			    else {
			        Iterator i = children.iterator();
			        while (i.hasNext()){
			            XMLElement xe = (XMLElement)i.next();
			            retList.addAll(xe.getChild(cannonicalName));
						isSuccess=true;
			        }
			    }
			    return;
			}
			
			String cannonNameLeft = cannonicalName.substring(nameIndex+1, cannonicalName.length());
			String currentName = cannonicalName.substring(0, nameIndex);
			if (children == null) {
			    children = root.getChild(currentName);
			    getEntries(cannonNameLeft, children, retList);
			}
			else {
			    Iterator i = children.iterator();
			    while (i.hasNext()){
			        XMLElement xe = (XMLElement)i.next();
			        getEntries(cannonNameLeft, xe.getChild(currentName), retList);
			    }
			}
			isSuccess=true;
		} finally {
			if(!isSuccess){
				log.warn("Unable to process tag-"+cannonicalName+"["+this.apiName+"]");	
			}
			
			//throw e;
		}
    }
    
    
    /**
     * @return Returns the root.
     */
    public XMLElement getRoot() {
        return root;
    }
    /**
     * @param root The root to set.
     */
    public void setRoot(XMLElement root) {
        this.root = root;
    }
}
