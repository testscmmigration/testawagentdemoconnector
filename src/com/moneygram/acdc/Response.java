/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc;

import java.util.HashMap;

/**
 * @author T007
 *
 */
public class Response {

    private HashMap setProperties = new HashMap();
    private Template template;

    public Response() {
        
    }
    
    public Response(Template template){
        this.template = template;
    }
    
    /**
     * @return Returns the template.
     */
    public Template getTemplate() {
        return template;
    }
    /**
     * @param template The template to set.
     */
    public void setTemplate(Template template) {
        this.template = template;
    }
    
    public void addProperty(String name, String value) {
        setProperties.put(name, value);
    } 
    
    public XMLElement process(Payload p) {
        return template.substitute(p, setProperties);
    }
    
    
    /**
     * 
     * @return 
     * @author 
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Response[");
        buffer.append("setProperties = ").append(setProperties);
        buffer.append(" template = ").append(template);
        buffer.append("]");
        return buffer.toString();
    }}

