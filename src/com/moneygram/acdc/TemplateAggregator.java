/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc;

/**
 * @author T007
 *
 */
public interface TemplateAggregator {
    public Template getTemplate(String templateName);
    public void addTemplate(Template t);
}
