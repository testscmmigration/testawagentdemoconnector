/*
 * Created on Jun 23, 2006
 *
 */
package com.moneygram.acdc;

import com.moneygram.acdc.exp.Expression;

/**
 * @author T007
 *
 */
public class MessageKey {
    private boolean defaultKey;
    private String apiVersion;
    private Expression rootExpression;
    private Response response;
    
    public MessageKey (boolean defaultKey, String apiVersion) {
        this.defaultKey = defaultKey;
        this.apiVersion = apiVersion;
    }
    
    /**
     * @return Returns the defaultKey.
     */
    public boolean isDefaultKey() {
        return defaultKey;
    }
    /**
     * @param defaultKey The defaultKey to set.
     */
    public void setDefaultKey(boolean defaultKey) {
        this.defaultKey = defaultKey;
    }
    
    /**
     * @param inputPayload
     * @return
     */
    public Payload process(Payload inputPayload) {
        XMLElement responseHM = response.process(inputPayload);
        String apiName = convertRequestAPIToResponse(inputPayload.getApiName());
        Payload retPayload = new Payload(apiName, inputPayload.getNamespace(), responseHM);
        
        return retPayload;
    }

    public String convertRequestAPIToResponse(String apiNameRequest){
        return apiNameRequest.substring(0, apiNameRequest.length()-"Request".length())+"Response";
    }
    
    /**
     * @param p
     * @return
     */
    public boolean matches(Payload p) {
    	if (rootExpression == null) {
    		return true;
    	} else {
            return rootExpression.evaluate(p);
    	}
    }
    
    /**
     * @return Returns the rootExpression.
     */
    public Expression getRootExpression() {
        return rootExpression;
    }
    /**
     * @param rootExpression The rootExpression to set.
     */
    public void setRootExpression(Expression rootExpression) {
        this.rootExpression = rootExpression;
    }
    
    
    /**
     * @return Returns the response.
     */
    public Response getResponse() {
        return response;
    }
    /**
     * @param response The response to set.
     */
    public void setResponse(Response response) {
        this.response = response;
    }
    
    
    /**
     * @return Returns the apiVersion.
     */
    public String getApiVersion() {
        return apiVersion;
    }
    /**
     * @param apiVersion The apiVersion to set.
     */
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
    /**
     * 
     * @return 
     * @author 
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("MessageKey[");
        buffer.append("apiVersion = ").append(apiVersion);
        buffer.append(" defaultKey = ").append(defaultKey);
        buffer.append(" response = ").append(response);
        buffer.append(" rootExpression = ").append(rootExpression);
        buffer.append("]");
        return buffer.toString();
    }}
