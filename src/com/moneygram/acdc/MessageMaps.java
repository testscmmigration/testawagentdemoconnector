/*
 * Created on Jun 23, 2006
 *
 */
package com.moneygram.acdc;

import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 * @author T007
 *
 */
public class MessageMaps implements TemplateAggregator{
    private HashMap templates = new HashMap();
    private HashMap messages = new HashMap();
	private static final Logger log = Logger.getLogger(MessageMaps.class);
    
    
    public Payload evaluateRequest(Payload p) {
        Message m = (Message)messages.get(p.getApiName());
        if (m == null ){
            log.warn("Failed - " + p.getApiName());
            return null;
        }
        return m.evaluate(p);
    }
    
    public Template getTemplate(String templateName) {
        return (Template)templates.get(templateName);
    }

    /**
     * @see com.moneygram.acdc.TemplateAggregator#addTemplate(com.moneygram.acdc.Template)
     */
    public void addTemplate(Template t) {
        templates.put(t.getTemplateName(), t);
    }
    
    public void addMessage(Message m) {
        messages.put(m.getApiName(), m);
    }
    
    
    /**
     * 
     * @return 
     * @author 
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("MessageMaps[");
        buffer.append("messages = ").append(messages);
        buffer.append(" templates = ").append(templates);
        buffer.append("]");
        return buffer.toString();
    }}
