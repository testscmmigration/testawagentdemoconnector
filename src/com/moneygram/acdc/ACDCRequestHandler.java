/*
 * Created on Jun 17, 2006
 *
 */
package com.moneygram.acdc;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.moneygram.acdc.util.SOAPUtility;
import com.moneygram.acdc.util.StaxUtil;

/**
 * @author t007
 *  
 */
public class ACDCRequestHandler implements Runnable {

    private static final String SOAP_NS = "http://schemas.xmlsoap.org/soap/envelope/";
    private static final String SOAP_NS_PREFIX = "soapenv"; 
    private static final String AC_PREFIX = "ac";
    public static final String XML_SCHEMA_NS = "http://www.w3.org/2001/XMLSchema-instance";
    private static final String XML_SCHEMA_NS_PREFIX = "xsi";
	
	private Socket connectedSocket;

    private HTTPRequestHeader header;

    private static Logger log = Logger.getLogger(ACDCRequestHandler.class);

    /**
     * @param socket
     */
    public ACDCRequestHandler(Socket socket) {
        this.connectedSocket = socket;
    }

    private List readHeader(BufferedInputStream in) throws IOException {
        ArrayList al = new ArrayList();
        int c;
        StringBuffer requestLine = new StringBuffer();

        while (true) {
            c = in.read();
            if (c == '\n') {
                String requestLineStr = requestLine.toString().trim();
                if (requestLineStr.length() == 0)
                    break;
                al.add(requestLineStr);
                requestLine.setLength(0);
            } else {
                if (c != '\r')
                    requestLine.append((char) c);
            }
        }

        return al;
    }

    private String readPayload(HTTPRequestHeader header, BufferedInputStream in)
            throws IOException {
        //Read the payload
        byte[] payloadBytes = new byte[header.getContentLength()];
        in.read(payloadBytes);
        String payload = null;

        if (!header.isCompressed()) {
            payload = new String(payloadBytes, header.getCharset());
        } else {
            //Handle compressed case.
        }
        return payload;
    }

    /**
     * http://www.oreilly.com/catalog/javanp2/chapter/ch11.html
     * 
     * @see java.lang.Runnable#run()
     */
    public void run() {
        BufferedInputStream bis = null;
        Writer out = null;
        XMLStreamReader parser = null;
        try {
            OutputStream raw = new BufferedOutputStream(connectedSocket
                    .getOutputStream());
            out = new OutputStreamWriter(raw);
            bis = new BufferedInputStream(connectedSocket.getInputStream());

            List al = readHeader(bis);

            //Read the header
            header = new HTTPRequestHeader(al);
            StringBuffer payload = new StringBuffer(readPayload(header, bis));

//            log.debug("REQUEST IN:\r\n"+header+payload.toString());

            //Strip all soap outta request
            payload = SOAPUtility.stipSOAP(payload);
			try {
				payload = prettyFormat(payload);
			} catch (Exception e) {
				//e.printStackTrace();
			}
            StringReader sr = new StringReader(payload.toString());
            XMLInputFactory factory = XMLInputFactory.newInstance();

            parser = factory.createXMLStreamReader(sr);
            //Advance to first tag.
            String requestName = null;
            String namespace = null;
            while (true) {
                int event = parser.next();
                if (event == XMLStreamConstants.START_ELEMENT) {
                    requestName = parser.getLocalName();
                    namespace = parser.getNamespaceURI();
                    break;
                }
            }

            if (requestName == null) {
                log.warn("Unknown payload (no initial key): ");
                return;
            }

            MessageMaps mms = ACDCMessageConfigXMLHandler.getMaps();
            Payload reqPayload = new Payload(requestName, namespace, header.getACAPIVersion());
            int e1 = parser.next();
            loadPayload(parser, reqPayload);
            Payload retPayload = mms.evaluateRequest(reqPayload);
            if (retPayload != null) {
                String outboundXML  = writePayload(retPayload);
                String httpCompleteResponse = HTTPRequestHeader.getHTTPHeaderResponse(outboundXML);
                
//                log.debug("REPLY OUT:\r\n"+httpCompleteResponse);
                
                out.write(httpCompleteResponse);
                out.flush();
            }
            
        } catch (Exception ex) {
//            ex.printStackTrace();
        	StackTraceElement[] eTrace = new Throwable().fillInStackTrace().getStackTrace();
            log.warn("Failed - " + eTrace[1].getClassName()
    				+ "-" +  eTrace[1].getMethodName() 
    				+ " caught Exception - " 
    				+ ex.getClass().getName() + ": " + ex.getMessage());
            log.warn("Failed");
        } finally {
            try {
                if (parser != null)
                    parser.close();
            } catch (Exception iex) {
            }

            try {
                if (bis != null)
                    bis.close();
            } catch (IOException iex) {
            }

            try {
                if (out != null)
                    out.close();
            } catch (IOException iex) {
            }

            try {
                if (connectedSocket != null)
                    connectedSocket.close();
            } catch (IOException iex) {
            }

        }
    }

    /**
     * Started at START_ELEMENT of payload node
     * 
     * @param parser
     * @param payload
     * @param currentName
     * @throws ACDCException
     */
    private void loadPayload(XMLStreamReader parser, Payload payload) throws ACDCException {
        StaxUtil.loadNamedTags(parser, payload.getRoot());
    }
    
    private String writePayload(Payload p ) throws ACDCException {
        Writer out = new StringWriter();
        
        XMLStreamWriter parser = null; 
        try {
	        XMLOutputFactory factory = XMLOutputFactory.newInstance();
	        parser = factory.createXMLStreamWriter(out);
	        parser.writeStartDocument();
	        writeSoapEnvelopeStart(parser);

	        parser.writeStartElement(AC_PREFIX, p.getApiName(), p.getNamespace());
	        parser.writeNamespace(AC_PREFIX, p.getNamespace());
	        
	        p.outputXML(parser, AC_PREFIX, p.getNamespace());

	        parser.writeEndElement(); //End the enclosing API
	        writeSoapEnvelopeEnd(parser);
	        parser.writeEndDocument();
	        
        }
        catch (XMLStreamException ex) {
            throw new ACDCException("Failed to write output", ex);
        }
        finally {
            if (parser != null) {
                try {
                    parser.close();
                } 
                catch (XMLStreamException e) {
                }
            }
            
            if (out != null) {
                try {
                    out.close();
                } 
                catch (IOException e) {
                }
            }
        }
        
        return out.toString();
    }

    private LinkedList tokenizeElementName(String elementName) {
        LinkedList retList = new LinkedList();
        StringTokenizer st = new StringTokenizer(elementName, ".");
        while (st.hasMoreTokens())
            retList.addLast(st.nextToken());
        
        return retList;
    }
    
    
    /**
     * @param parser
     */
    private void writeSoapEnvelopeStart(XMLStreamWriter parser) throws XMLStreamException{
        parser.writeStartElement(SOAP_NS_PREFIX, "Envelope", SOAP_NS);
        parser.writeNamespace(XML_SCHEMA_NS_PREFIX, XML_SCHEMA_NS);
        parser.writeNamespace(SOAP_NS_PREFIX, SOAP_NS);
        parser.writeStartElement(SOAP_NS_PREFIX, "Body", SOAP_NS);
    }
    
    /**
     * @param parser
     */
    private void writeSoapEnvelopeEnd(XMLStreamWriter parser) throws XMLStreamException {
        
        parser.writeEndElement();
        parser.writeEndElement();
    }

    public static StringBuffer prettyFormat(StringBuffer input) {
        try
        {
            Source xmlInput = new StreamSource(new StringReader(input.toString()));
            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
             
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(xmlInput, xmlOutput);
            return new StringBuffer(xmlOutput.getWriter().toString());
        }
        catch (Throwable e)
        {
            try
            {
                Source xmlInput = new StreamSource(new StringReader(input.toString()));
                StringWriter stringWriter = new StringWriter();
                StreamResult xmlOutput = new StreamResult(stringWriter);
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", String.valueOf(2));
                transformer.transform(xmlInput, xmlOutput);
                return new StringBuffer(xmlOutput.getWriter().toString());
            }
            catch(Throwable t)
            {
                return input;
            }
        }
    }
}
