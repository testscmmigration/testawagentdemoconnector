/*
 * Created on Jun 15, 2006
 *
 */
package com.moneygram.acdc;

import java.io.IOException;
import java.net.ServerSocket;

import org.apache.log4j.Logger;

import edu.emory.mathcs.backport.java.util.concurrent.ExecutorService;
import edu.emory.mathcs.backport.java.util.concurrent.Executors;

/**
 * @author T007
 *
 */
public class AgentConnectDemoConnector {
	public static final Logger log = Logger.getLogger(AgentConnectDemoConnector.class);
	
	public static final String PORT_KEY = "-port";
	public static final String MESSAGE_CONFIG_FILE_KEY = "-msgConfig";
	
	public static final int DEFAULT_PORT = 4545;
    public static final String DEFAULT_MSG_CONFIG_FILE  = "configuration/configuration.txt";
	public static final String VERSION = "12.11.03";
	
	private final ServerSocket serverSocket;
	private final ExecutorService execService;
	
	
	public AgentConnectDemoConnector(int port) throws IOException{
		serverSocket = new ServerSocket(port);
        execService = Executors.newCachedThreadPool();
	}

	public void serve() {
		//Run forever 
		while(true)  {
			try {
				execService.execute(new ACDCRequestHandler(serverSocket.accept()));
			}
			catch (IOException ex) {
				execService.shutdown();
			}
		}
		
	}
	
    public static void main(String[] args) {
    	try {
    	    int port = DEFAULT_PORT;
    	    String configFile = DEFAULT_MSG_CONFIG_FILE;
    	    for (int i = 0; i < args.length; ++i) {
    	        String curArg = args[i];
    	        if (curArg.equalsIgnoreCase(PORT_KEY)) {
    	            ++i;
    	        	port = Integer.parseInt(args[i]);
    	        }
    	        else
    	        if (curArg.equalsIgnoreCase(MESSAGE_CONFIG_FILE_KEY)){
    	            ++i;
    	            configFile = args[i]; 
    	        }
    	        else {
    	            throw new ACDCException("Uknown program parameter: "+curArg);
    	        }
    	            
    	    }
    	    log.info("Starting daemon" +
    	    		"\r\n\tVersion: "+VERSION+
    	    		"\r\n\tport: "+port+
    	    		"\r\n\tconfig file: "+configFile);
    	    
    	    ACDCMessageConfigXMLHandler.getMaps(configFile);
	    	AgentConnectDemoConnector acdc = new AgentConnectDemoConnector(port);
	    	acdc.serve();
    	}
    	catch (ACDCException ex) {
    	    log.error("Failed to load configurations", ex);
    	}
    	catch (IOException iex) {
    		log.fatal("Failed - Network/Socket problem", iex);
    	}
    }
}
