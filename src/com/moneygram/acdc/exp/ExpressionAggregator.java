/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc.exp;

import java.util.Iterator;


/**
 * @author T007
 *
 */
public interface ExpressionAggregator extends Expression {
    public void addExpression(Expression ex);
    public Iterator getExpressions();
    
}
