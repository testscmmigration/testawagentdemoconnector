/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc.exp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.moneygram.acdc.Payload;


/**
 * @author T007
 *
 */
public class AndExpression implements ExpressionAggregator{
    private List subExpressions = new ArrayList();
    
    
    /**
     * @see com.moneygram.acdc.exp.Expression#evaluate(com.moneygram.acdc.Payload)
     */
    public boolean evaluate(Payload p) {
        Iterator iter = getExpressions();
        
        while (iter.hasNext()) {
            Expression exp = (Expression) iter.next();
            if (exp.evaluate(p) == false)
                return false;
        }
        
        return true;
    }
    
    
    /**
     * @see com.moneygram.acdc.exp.ExpressionAggregator#addExpression(com.moneygram.acdc.Expression)
     */
    public void addExpression(Expression ex) {
        subExpressions.add(ex);
    }
    /**
     * @see com.moneygram.acdc.exp.ExpressionAggregator#getExpressions()
     */
    public Iterator getExpressions() {
        return subExpressions.iterator();
    }
}
