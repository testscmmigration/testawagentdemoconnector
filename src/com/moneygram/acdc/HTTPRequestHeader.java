/*
 * Created on Jun 19, 2006
 *
 */
package com.moneygram.acdc;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * @author T007
 *
 */
public class HTTPRequestHeader {
    public static final Logger log = Logger.getLogger(HTTPRequestHeader.class);
    
    public static final String HTTP_POST_COMMAND  = "POST";
    
    public static final String HTTP_CONTENT_LENGTH = "Content-Length:";
    public static final String HTTP_CONTENT_TYPE = "Content-Type:";
    public static final String HTTP_CONTENT_TYPE_LCT = "Content-type:";
    public static final String HTTP_CHARSET_TYPE = "charset=";
    
    public static final String HTTP_CONTENT_TYPE_XML = "text/xml";
    public static final String HTTP_CONTENT_TYPE_GZIP = "application/x-gzip";
    
    private static final int NOT_INITED = -2; 
    public static final int NOT_SET = -1;

    private static final String NOT_INITED_STR = "NOINITED";
    public static final String NOT_SET_STR="NOTSET";
    
    List header;

    private int contentLength = NOT_INITED;
    private String contentType = NOT_INITED_STR;
    private String charset = NOT_INITED_STR;
    private String apiVersion = NOT_INITED_STR;
    
    public HTTPRequestHeader(List header) {
        this.header=header;
    }
    
    private static final String AGENT_CONNECT_URL_VERSION_PREFIX = "AgentConnect";  
    public String getACAPIVersion() {
        if (apiVersion == NOT_INITED_STR) {
            String valuePart = findStringValue(HTTP_POST_COMMAND);
            int i =  valuePart.indexOf(AGENT_CONNECT_URL_VERSION_PREFIX);
            int start = i+AGENT_CONNECT_URL_VERSION_PREFIX.length();
            apiVersion = valuePart.substring(start, start + 4);
        }
        
        return apiVersion;
    }
    
    public int getContentLength() {
        if (contentLength == NOT_INITED) {
            contentLength = findIntValue(HTTP_CONTENT_LENGTH);
        }
        return contentLength;
    }
    
    public String getContentType() {
        if (contentType.equals(NOT_INITED_STR)) {
            String combinedType = findStringValue(HTTP_CONTENT_TYPE);
            if (combinedType.equals(NOT_INITED_STR)) {
                combinedType = findStringValue(HTTP_CONTENT_TYPE_LCT);
            }
            
            String[] splits = combinedType.split(";");
            contentType = splits[0];
            if (splits.length == 2) {
                String[] charSetSplit = splits[1].split("=");
                if (charSetSplit.length == 2) {
					if (charSetSplit[1].startsWith("\"") && charSetSplit[1].endsWith("\"")) {
						charset = charSetSplit[1].substring(1, charSetSplit[1].length() - 1);
					} else {
						charset = charSetSplit[1];
					}
                }
            }
            else
                charset = NOT_SET_STR;
        }
        
        return contentType;
    }

    public String getCharset() {
        if (charset.equals(NOT_INITED_STR))
            getContentType();
        
        return charset;
    }
    
    private int findIntValue(String valueString){
        Iterator iter = header.iterator();
        
        while (iter.hasNext()) {
            String entry = (String) iter.next();
            
            int index = entry.indexOf(valueString);
            if (index != -1) {
                String valuePart = entry.substring(valueString.length());
                if (valuePart != null) {
                    valuePart = valuePart.trim();
                    try {
                        return Integer.parseInt(valuePart);
                    }
                    catch (NumberFormatException nfex) {
                        log.debug("Could not process int", nfex);
                    }
                }
            }
        }
        return NOT_SET;
    }
    
    private String findStringValue(String valueString) {
        Iterator iter = header.iterator();
        
        while (iter.hasNext()) {
            String entry = (String) iter.next();
            
            int index = entry.indexOf(valueString);
            if (index != -1) {
                String valuePart = entry.substring(valueString.length());
                if (valuePart != null) {
                    valuePart = valuePart.trim();
                    try {
                        return valuePart;
                    }
                    catch (NumberFormatException nfex) {
                        log.debug("Could not process string", nfex);
                    }
                }
            }
        }
        return NOT_INITED_STR;
    }
    
    public boolean isCompressed() {
        return getContentType().equals(HTTP_CONTENT_TYPE_GZIP);
    }
    
    /**
     * @param charset The charset to set.
     */
    public void setCharset(String charset) {
        this.charset = charset;
    }
    /**
     * @param contentLength The contentLength to set.
     */
    public void setContentLength(int contentLength) {
        this.contentLength = contentLength;
    }
    /**
     * @param contentType The contentType to set.
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    
    public static String getHTTPHeaderResponse(String content) {
        StringBuffer sb = new StringBuffer();
        sb.append("HTTP/1.1 200 OK\r\n");
        sb.append("Server: AgentConnectDemoConnector\r\n");
        sb.append(HTTP_CONTENT_TYPE+" text/xml; charset=utf-8\r\n");
        sb.append("Content-Language: en-US\r\n");
//        sb.append(HTTP_CONTENT_LENGTH + " " + content.length()+"\r\n");
        sb.append("Transfer-Encoding: chunked\r\n");
        sb.append("\r\n");
        sb.append(Integer.toHexString(content.length()));
        sb.append("\r\n");
        sb.append(content);
        sb.append("\r\n0\r\n\r\n");
        return sb.toString();
    }
    
    /**
     * 
     * @return 
     * @author 
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("HTTPRequestHeader[");
        buffer.append("apiVersion = ").append(apiVersion);
        buffer.append(" charset = ").append(charset);
        buffer.append(" contentLength = ").append(contentLength);
        buffer.append(" contentType = ").append(contentType);
        buffer.append("]");
        return buffer.toString();
    }}
