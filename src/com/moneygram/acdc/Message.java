/*
 * Created on Jun 23, 2006
 *
 */
package com.moneygram.acdc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * @author T007
 *
 */
public class Message implements TemplateAggregator{
    public static final String ALL_VERSION = "ALL";
    
    private String apiName;
    private HashMap templates = new HashMap();
    private HashMap messageKeys = new HashMap();
    
    public Message(String apiName) {
        this.apiName = apiName;
    }
    
    public Payload evaluate(Payload p) {
        MessageKey defKey = null;
        
        String apiVersion = p.getApiVersion();
        List list = null;

        if (apiVersion != null) {
	        list = (List)messageKeys.get(p.getApiVersion());
        }
        
        if (apiVersion == null || list == null) {
	        //If the specific version is not found, try to find in any version.
            list = (List)messageKeys.get(ALL_VERSION);
            if (list == null)
                return null;
        }
        
        for (Iterator iter = list.iterator(); iter.hasNext();) {
            MessageKey messageKey = (MessageKey) iter.next();
            if (messageKey.isDefaultKey())
                defKey = messageKey;
            
            if (messageKey.matches(p)) {
                return messageKey.process(p);
            }
        }
        
        if (defKey != null) {
            return defKey.process(p);
        }
        
        return null;
    }


    public Template getTemplate(String templateName) {
        return (Template)templates.get(templateName);
    }

    /**
     * @see com.moneygram.acdc.TemplateAggregator#addTemplate(com.moneygram.acdc.Template)
     */
    public void addTemplate(Template t) {
        templates.put(t.getTemplateName(), t);
    }    
    
    
    public void addMessageKey(MessageKey mk) {
        String apiVersion = mk.getApiVersion()==null ? ALL_VERSION : mk.getApiVersion();
        
        List list = (List)messageKeys.get(apiVersion);
        if (list == null) {
            list = new ArrayList();
            messageKeys.put(apiVersion, list);
        }
        list.add(mk);
    }
    
    /**
     * @return Returns the apiName.
     */
    public String getApiName() {
        return apiName;
    }
    /**
     * @param apiName The apiName to set.
     */
    public void setApiName(String apiName) {
        this.apiName = apiName;
    }
    
    
    /**
     * 
     * @return 
     * @author 
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Message[");
        buffer.append("apiName = ").append(apiName);
        buffer.append(" messageKeys = ").append(messageKeys);
        buffer.append(" templates = ").append(templates);
        buffer.append("]");
        return buffer.toString();
    }}
